package amazon.s3;

import org.junit.After;
import org.junit.Test;
import org.springframework.boot.test.EnvironmentTestUtils;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;

import static junit.framework.TestCase.assertNotNull;

public class S3AutoConfigurationTest {

    private AnnotationConfigApplicationContext context;

    @After
    public void tearDown() {
        if (this.context != null) {
            this.context.close();
        }
    }

    @Test
    public void defaultAmazonS3Template() {
        load(EmptyConfiguration.class, "amazon.s3.default-bucket=demo001kevin",
                "amazon.aws.access-key-id=AKIAJRPFKPJDZ7X3TM3A",
                "amazon.aws.access-key-secret=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");

        AmazonS3Template amazonS3Template = this.context.getBean(AmazonS3Template.class);
        assertNotNull(amazonS3Template);
    }

    @Configuration
    static class EmptyConfiguration {
    }

    private void load(Class<?> config, String... environment) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        EnvironmentTestUtils.addEnvironment(applicationContext, environment);
        applicationContext.register(config);
        applicationContext.register(S3AutoConfiguration.class);
        applicationContext.refresh();
        this.context = applicationContext;
    }
}
